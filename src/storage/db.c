/*
 * Copyright (C) 2016  Kim Nordmo
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY: without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have receeived a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <sqlite3.h>
#include "db.h"

static bool table_exists(const sqlite3 * db, const char *tablename);

sqlite3 *db_open(const char *filepath)
{
    sqlite3 *db;
    int rc =
        sqlite3_open_v2(filepath, &db,
                        SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);

    if (rc != SQLITE_OK) {
        fprintf(stderr, "%s\n", sqlite3_errmsg(db));
        errno = sqlite3_errcode(db);
        db_close(db);
        return NULL;
    }

    return db;
}

void db_close(sqlite3 * db)
{
    sqlite3_close_v2(db);
    db = NULL;
}

char *db_version(const sqlite3 * db)
{

}
